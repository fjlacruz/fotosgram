import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { environment } from '../../environments/environment';
import { NavController } from '@ionic/angular';
import { Usuario } from '../interfaces/interfaces';


const URL = environment.url;

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  token: string = null;
  private usuario: Usuario = {};
  constructor(private http: HttpClient, private storage: Storage, private navCtrl: NavController) { }


  login(email: string, password: string) {
    const data = { email, password };
    return new Promise(resolve => {
      this.http.post(`${URL}/login`, data)
        .subscribe(async resp => {

          if (resp['ok']) {
            await this.guardarToken(resp['token']);
            console.log('login:', resp['token'])
            resolve(true);
          } else {
            this.token = null;
            this.storage.clear();
            resolve(false);
          }
        });
    });
  }
  registro(usuario: Usuario) {
    return new Promise(resolve => {
      this.http.post(`${URL}/save-user`, usuario)
        .subscribe(async resp => {
          console.log(resp);

          if (resp['ok']) {
            await this.guardarToken(resp['token']);
            resolve(true);
          } else {
            this.token = null;
            this.storage.clear();
            resolve(false);
          }
        });
    });
  }

  async guardarToken(token: string) {

    this.token = token;
    await this.storage.set('token', token);

    await this.validaToken();


  }

  async cargarToken() {
    this.token = await this.storage.get('token') || null;
  }


  async validaToken(): Promise<boolean> {

    await this.cargarToken();

    if (!this.token) {
      this.navCtrl.navigateRoot('/login');
      return Promise.resolve(false);
    }


    return new Promise<boolean>(resolve => {

      const headers = new HttpHeaders({
        'Authorization': this.token
      });

      //console.log('token:', this.token)

      this.http.get(`${URL}/user`, { headers })
        .subscribe(resp => {
          //console.log('resp:', resp['user'])

          if (resp['ok']) {
            this.usuario = resp['user'];
            //console.log('usuario:', this.usuario)
            resolve(true);
          } else {
            this.navCtrl.navigateRoot('/login');
            resolve(false);
          }
        });
    });
  }

  getUsuario() {
    //console.log('token:', this.token)

    if (!this.usuario.nombre) {
      this.validaToken();
    }
    return { ...this.usuario };
  }

  actualizarUsuario(usuario: Usuario) {
    const headers = new HttpHeaders({
      'Authorization': this.token
    });
    console.log('actualizarUsuario:', this.token)
    //console.log(this.token)
    return new Promise(resolve => {
      this.http.put(`${URL}/update-user`, usuario, { headers })
        .subscribe(resp => {
          console.log(resp);
          if (resp['ok']) {
            this.guardarToken(resp['token']);
            console.log(resp['token']);
            resolve(true);
          } else {
            resolve(false);
          }
        });
    });
  }


  logout() {
    this.token = null;
    this.usuario = null;
    this.storage.clear();
    this.navCtrl.navigateRoot('/login', { animated: true });
  }


}
