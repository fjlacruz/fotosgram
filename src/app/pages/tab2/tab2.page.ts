import { Component } from '@angular/core';
import { PostsService } from '../../services/posts.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

declare var window: any;

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  tempImages: string[] = [];

  post = {
    mensaje: '',
    coords: null,
    posicion: false

  };
  cargandoGeo = false;


  constructor(private postPoisteservice: PostsService, private geolocation: Geolocation, private camera: Camera) { }

  crearPost() {
    //console.log(this.post)
    this.postPoisteservice.crearPost(this.post)
  }

  getGeo() {

    if (!this.post.posicion) {

      this.post.coords = null;
      return
    }
    this.cargandoGeo = true

    this.geolocation.getCurrentPosition().then((resp) => {
      // resp.coords.latitude
      // resp.coords.longitude
      this.cargandoGeo = false

      const coords = `${resp.coords.latitude},${resp.coords.longitude}`;

      this.post.coords = coords;

      console.log(coords)
    }).catch((error) => {
      this.cargandoGeo = false
    });
    console.log(this.post)
  }

  camara() {
    const options: CameraOptions = {
      quality: 60,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType: this.camera.PictureSourceType.CAMERA
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      //let base64Image = 'data:image/jpeg;base64,' + imageData;
      const img = window.Ionic.WebView.convertFileSrc(imageData)
      console.log(img)
      this.tempImages.push(img)
    }, (err) => {
      // Handle error
    });
  }

}
