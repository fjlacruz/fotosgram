import { Component, OnInit } from "@angular/core";
import { PostsService } from "../../services/posts.service";
import { Post } from "../../interfaces/interfaces";
import { EventEmitter } from 'protractor';

@Component({
  selector: "app-tab1",
  templateUrl: "tab1.page.html",
  styleUrls: ["tab1.page.scss"],
})
export class Tab1Page implements OnInit {
  posts: Post[] = [];
  habilitado = true
  constructor(private postService: PostsService) { }

  ngOnInit() {
    this.siguientes();
  }

  recargar(event) {
    this.siguientes(event, true);
    this.habilitado = true;
    this.posts = [];
  }
  siguientes(event?, pull: boolean = false) {
    this.postService.getPost(pull).subscribe((resp) => {
      //console.log(resp.posts);
      this.posts.push(...resp.posts);
      //console.log(this.posts)

      if (event) {
        event.target.complete();
        if (resp.posts.length === 0) {
          this.habilitado = false;
        }
      }

    });
  }
}
