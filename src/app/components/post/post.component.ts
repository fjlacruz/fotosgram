import { Component, OnInit, Input } from "@angular/core";
import { Post } from "../../interfaces/interfaces";
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: "app-post",
  templateUrl: "./post.component.html",
  styleUrls: ["./post.component.scss"],
})
export class PostComponent implements OnInit {
  @Input() post: Post = {};
  img = 'http://localhost:3700/api/get-user-avatar/5VHzEs6O8_pDW0-fN-I74WwE.png';
  ruta = 'http://localhost:3700/api/get-user-avatar/5VHzEs6O8_pDW0-fN-I74WwE.png'

  ///http://localhost:8100//src/assets/avatars/av-1.png

  slideSoloOpts = {
    allowSlideNext: false,
    allowSlidePrev: false,
  };

  constructor(private sanitizer: DomSanitizer,) { }

  ngOnInit() {




  }

  getImgContent() {
    let imgs = this.sanitizer.bypassSecurityTrustUrl(this.img);
    return this.sanitizer.bypassSecurityTrustUrl(this.img);
  }
}
