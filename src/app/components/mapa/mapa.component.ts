import { Component, OnInit, Input, ViewChild } from '@angular/core';


declare var mapboxgl: any;

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.scss'],
})
export class MapaComponent implements OnInit {


  @Input() coords: string;
  @ViewChild('mapa') mapa;
  @ViewChild('myDiv') child: HTMLElement;
  constructor() { }

  ngOnInit() {
    console.log('mapa:', this.mapa)
    console.log("ngAfterViewInit", this.child);


    const latLng = this.coords.split(',');
    const lat = Number(latLng[0]);
    const lng = Number(latLng[1]);


    mapboxgl.accessToken = 'pk.eyJ1IjoiamxhY3J1eiIsImEiOiJja2RieTQ3MXExa3BxMnJsdm10c2NyY2RqIn0.CvyAYziXOHsoMO0tKojwzw';
    var map = new mapboxgl.Map({
      container: 'map',
      // container: this.mapa.nativeElement.value,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [lng, lat],
      zoom: 150
    });

    const marker = new mapboxgl.Marker()
      .setLngLat([lng, lat])
      .addTo(map)

  }


}
